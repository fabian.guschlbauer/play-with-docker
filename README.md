# Play-with-Docker

This repository provides a simple docker compose file used in my blog post "[Play with Docker (PWD) - An Amazing One-Click Docker Playground Project](https://www.guschlbauer.dev/play-with-docker-pwd-an-amazing-one-click-docker-playground-project/)"

You can start the PWD showcaseing application using:
